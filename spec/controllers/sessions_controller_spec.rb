require 'spec_helper'

describe SessionsController do

  before(:each) do
    OmniAuth.config.test_mode = true
    OmniAuth.config.mock_auth[:facebook] = {
        'uid' => '12345',
        'provider' => 'facebook',
        'info' => {
          'name' => 'Bob'
        }
      }
  end

  describe "GET 'new'" do
    it "przekierowuje uzytkownika do autentykacji" do
      get 'new'
      assert_redirected_to '/auth/facebook'
    end
  end

  describe "logowanie przez facebook" do
    it "loguje nowego uzytkonika z komunikatem" do
      @user = FactoryGirl.create(:user)
      visit '/signin'
      page.should have_content('Logged in as Bob')
     end
    it "przekierowuje uzytkownika, ktory sie wylogowal do strony glownej" do
      @user = FactoryGirl.create(:user)
      visit '/signout'
      page.should have_content('Signed out!')
      current_path.should == '/'
    end
  end

end
